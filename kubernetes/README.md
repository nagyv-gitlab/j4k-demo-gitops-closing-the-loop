# Kubernetes manifests

The GitLab Kubernetes Agent supports pull-based deployments using vanilla Kubernetes manifests. This directory contains sub-directories and single-file application definitions as pure kubernetes manifests. 

The applications in `/packages` render/hydrate their corresponding manifests here.
