#!/bin/sh

if [ $# -ne 2 ]
  then
    echo "Usage: $0 ignored/my-secret.yaml output-dir/"
    echo "This script requires two arguments"
    echo "The first argument should be the unsealed secret"
    echo "The second argument should be the directory to output the sealed secret"
  exit 1
fi


SECRET_FILE=$(basename $1)

kubeseal --format=yaml --cert=sealed-secrets.pub.pem < $1 > "$2/SealedSecret.${SECRET_FILE}"

echo "Created file $2/SealedSecret.${SECRET_FILE}"