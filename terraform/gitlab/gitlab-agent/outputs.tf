output "agent_id" {
  value     = module.gitlab_kubernetes_agent_registration.agent_id
}

output "token_secret" {
  value     = module.gitlab_kubernetes_agent_registration.token_secret
  sensitive = true
}

output "runner_token" {
  value = data.gitlab_project.current_project.runners_token
  sensitive = true
}
