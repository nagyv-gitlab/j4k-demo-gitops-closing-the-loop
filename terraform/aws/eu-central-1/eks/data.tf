data "terraform_remote_state" "vpc" {
  backend = "http"
  config = {
    address = "${var.remote_address_base}/vpc"
    username = var.gitlab_username
    password = var.gitlab_password
  }
}
