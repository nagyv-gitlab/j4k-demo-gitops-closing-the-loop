# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gavinbunney/kubectl" {
  version     = "1.10.0"
  constraints = "~> 1.0"
  hashes = [
    "h1:Sh9IZV2EiEJP240yjK4hn6EZt7aSxcKSonsHEFGGbdg=",
    "zh:0786e6cb375e4e6a70220bb67fc3de80c8c30dcb00c0f4f0ec7bb10404a120db",
    "zh:577347a8334c8cd13215608780e03b77615d211fac64ad6e4356b7f4bb160022",
    "zh:7d3347690a0b68dca54ae5cc90877cf82069f7ef13517668b17fd37f49c91e8c",
    "zh:7f4eeae41b22de803ea7bf8977226c2bc0baaf204a4a2a05c421d9358c907808",
    "zh:8db7a6550374918109d6f445c6c196f02ea3fa2029b882eca186d6e13bd1e4ce",
    "zh:9c93ad71c3039463cf4345acb781c68d7ce82fe8f8495a94a6b588bf87259e51",
    "zh:ee94ff2448caee374f3a3e888568d7ff48e6b9438df76f6eb72efa1aadc6391b",
  ]
}

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.6.0"
  constraints = "~> 3.6.0"
  hashes = [
    "h1:icNPivGM5+Rilvii04ygDEycpSGRzQ4oH+4oXKfhd8M=",
    "zh:08a915f21564737090eea015f47d7dd5c2d1a5f63b49211937534f643f47b8ca",
    "zh:145527bed51ec8070a85e967395fcd9aa32c97fbf2cf42747397cef6f1388213",
    "zh:1e3c8c26af5076b9067ac9f6f71b188c25838db42e8d6c11128322d6c4655982",
    "zh:5a795890e78121f7b8d8a3ae6fdc6a6f69083465b6a2073c9c0f0564dad4f7ec",
    "zh:765219c271e8af16fd436cf88e32079379b035d92bca0df556dee9adf9c9152f",
    "zh:7804d99ba61f74db1a4a911d181e7f8cee62c41658fe424cfa28362d6bbe8784",
    "zh:85e51fd6402de3e842e75ea02dc79cc3903fd030bd3d312bfcb78a8a61b8dfaa",
    "zh:8b13a75dcd932dc43616d058eeae60fada70bc93b0deb7152a4aba55d365e5b0",
    "zh:8d80014cc5acf9c4c64cd73e8f15b418b52cc54e112b1ebb49fdefa2ad8e13eb",
    "zh:b25129758f8aa6835dd22101f945abed6430d457659d20d2638eff27ca71bddd",
    "zh:b35e089343a6185db0196273517fcfe98fbd13d6ac1901b49d1d24b2b39b2ff1",
    "zh:cd928bc0e5d42fb966fd583cb8c46169cf80d35a57859db949d0c0b802fef35d",
    "zh:f8af5be58d41afcda522926e3768728fe412a87dbacca7ecb02341b854bf798f",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.37.0"
  constraints = ">= 3.15.0"
  hashes = [
    "h1:RvLGIfRZfbzY58wUja9B6CvGdgVVINy7zLVBdLqIelA=",
    "zh:064c9b21bcd69be7a8631ccb3eccb8690c6a9955051145920803ef6ce6fc06bf",
    "zh:277dd05750187a41282cf6e066e882eac0dd0056e3211d125f94bf62c19c4b8b",
    "zh:47050211f72dcbf3d99c82147abd2eefbb7238efb94d5188979f60de66c8a3df",
    "zh:4a4e0d070399a050847545721dae925c192a2d6354802fdfbea73769077acca5",
    "zh:4cbc46f79239c85d69389f9e91ca9a9ebf6a8a937cfada026c5a037fd09130fb",
    "zh:6548dcb1ac4a388ed46034a5317fa74b3b0b0f68eec03393f2d4d09342683f95",
    "zh:75b4a82596aa525d95b0b2847fe648368c6e2b054059c4dc4dcdee01d374b592",
    "zh:75cf5cc674b61c82300667a82650f56722618b119ab0526b47b5ecbb4bbf49d0",
    "zh:93c896682359039960c38eb5a4b29d1cc06422f228db0572b90330427e2a21ec",
    "zh:c7256663aedbc9de121316b6d0623551386a476fc12b8eb77e88532ce15de354",
    "zh:e995c32f49c23b5938200386e08b2a3fd69cf5102b5299366c0608bbeac68429",
  ]
}

provider "registry.terraform.io/hashicorp/helm" {
  version     = "2.1.2"
  constraints = "~> 2.0"
  hashes = [
    "h1:UVuNjmuEM4ZVtItbh1QRGulkBWxDY929roxFQhEf9Ks=",
    "zh:09bd2b6f33a040c3fd59d82c9768b886b8c82163e31ec92dc1b747229d0548df",
    "zh:09f209fa57ad5d01f04c458f1719b42958ca5e0fc2eca63d9ec29f92c77a29f8",
    "zh:0bfc627539500ffb2a41a2f8a5ea7f6fb1d76367b11bbf9489b483b9e8dfff8f",
    "zh:0c0fef5587a5e927d15f9f4cc13cd0620b138238f9a422490fe9ea2bf086b61a",
    "zh:187f99648fad2b84d49cdd372f8f6cedbf06e13411b3f1ff66708f66852d7855",
    "zh:3d9ae08f8a99b19e80bd27708aecf592c28c92da66fd60189dfd7dce4d7da93c",
    "zh:60b767109362c616b2e6386bfb08581b03bc3e528920444e52b16743f5a180d6",
    "zh:729db42ed49d91c9b51eb602b9253e6ed6b3ab613c42deefc14996c9a8ee8ae4",
    "zh:8401f3bf6d69ce43eb14911823c7e5cbb273cf564508043cd04fb064c30a3e1a",
    "zh:91139b492ce1f41847017349ea49f9441b7cf70762c8d1c32a6a909e25ed10c1",
    "zh:98fca606a539510edc94dcad8069a321e6a42df90e483f58df03b305726d9220",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.1.0"
  constraints = "~> 2.0"
  hashes = [
    "h1:L/3XfqLQ4bS1PjH/FksJPm+MYIOxCwn97ozbfSwg/VQ=",
    "zh:22e2bcef08fb7f97ed503a27e3725d9d14fdd09fe3aa144fae8a7f78ed27856a",
    "zh:2380cc2a91239b80ea380af8a7fcdcc7396f5213a71a251a5505c962ac6cb9c2",
    "zh:496ea2818d5480590ada763672be051f4e76dc12c6a61fde2faa0c909e174eb7",
    "zh:4e5b6c230d9a8da8a0f12e5db198f158f2c26432ad8e1c6ac22770ce7ec39118",
    "zh:55ad614beffda4cdc918ad87dca09bb7b961f12183c0923230301f73e23e9665",
    "zh:6849c52899091fa2f6714d8e5180a4affffc4b2ad03dc2250043d4b32049e16e",
    "zh:7a6f0d9da5172b3770af98d59263e142313a8b2c4048271893c6003493ad1c89",
    "zh:7c97fb24e60c41fa16f6305620d18ae51545c329f46f92988493a4c51a4e43e5",
    "zh:a08111c4898544c40c62437cc28798d1f4d7298f61ddaf3f48dddec042d3519f",
    "zh:be7493bff6b9f95fe203c295bfc5933111e7c8a5f3bd9e9ae143a0d699d516f8",
    "zh:e4c94adc65b5ad5551893f58c19e1c766f212f16220087ca3e940a89449ac285",
  ]
}

provider "registry.terraform.io/sullivtr/graphql" {
  version = "1.4.6"
  hashes = [
    "h1:u4m/uBaHave0VJWMJXaKI1G4lfDJZuC7VIO5+OzEFCY=",
    "zh:1f3dca3a6d957ef90b07e4c4eec169387fc8124f2eb17335290f315cb9638cf2",
    "zh:43b91bdbe888d04fe14ebd7aadb58b201158e0045f154972587bf93fab9e0112",
    "zh:5c93fc774e1a071e151038e39727f449db31eb49d49c50f9df0783bf1e6de295",
    "zh:5debd5ac1b149b128c272eaa82fd2faea8eca68a0ca8418c8f51b13f3000daa5",
    "zh:645df8ffd7ae88f40e70f0e7d82c50ec89d7b9fe754b0cb04a3bdd700d520be3",
    "zh:815eb2d2fd61176d0de2fb2b37f7228da72ea9fe5542d634256babc975b59103",
    "zh:8445d0e6191316bbb58ecd37807a540703bfbf724d97888a786a0046529aa148",
    "zh:8514b68a774403728c76ce43732d1a46cc44541789c8999576d0f1c10cbc8a40",
    "zh:b9f0c62fd26b379d3bbbf3f80a66fbccd7deb04e8eebd06ea570ce02b69a75db",
    "zh:d971abfb5d3c3c500eab2fdd5fdb5ce8d69177b2f97535abdb1f3b6b9788b1f7",
    "zh:e684924ee1c3975f49bbbcec54d40b207890475df100781d8d2ad3eed1f997f2",
    "zh:ea152ca7f1e80ca9a99e3bad543161751a6b062684f602e2a8fff6e62f4e1245",
  ]
}
