output "cluster" {
  value = {
    status = civo_kubernetes_cluster.dev-cluster.status
    master_ip = civo_kubernetes_cluster.dev-cluster.master_ip
    dns_entry = civo_kubernetes_cluster.dev-cluster.dns_entry
  }
}