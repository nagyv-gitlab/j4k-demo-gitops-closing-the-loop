# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/civo/civo" {
  version     = "0.10.4"
  constraints = "0.10.4"
  hashes = [
    "h1:vFA6Nu/gGDZ8vxvLpn5B2OpzInaMEL2uII0xEac97SM=",
    "zh:07d47215b982a2a18458dc40cb88c394233792c58fd2e4395edcfac0d44cf548",
    "zh:21a9bc58ac67a2e383d6a4d09d09b3170a469a38b943af28e0b98040c31e9a0a",
    "zh:239dab9261062520009506d599c35513609bdb1dc8caa092d68a7870ffb2dba6",
    "zh:294e64f7b91a132961b72aa471e4cefaaafd2f7f0e698d8e084728658cba2982",
    "zh:40b32e5b39cfddb8757f36bc18c124c73f0fed972b8ae27ac73bd8961b4ab86e",
    "zh:4f78937478737b2dbcac60502f3e37a8c9918bd74b6626904a3029b4d99afeef",
    "zh:63b82eebe9d005a280e3eb0abfb03d7e47833e94809f4bce13d1c53ef96d25dc",
    "zh:978cc7366a9f4545e8401701772f0cf4f36db283058e439a4eeaab33549456b1",
    "zh:d51018e425b63fdb490d734bf0f6ff49dde00fe52ef4d9e73e18fe2df4ff93d6",
    "zh:d72e8ceb6d87fe172e8d6648f7eda6e55e14d8b6628dd065ce1e9f1aa2693e8d",
    "zh:ea410cb9b50f646b8c269e19cb5f877e57d339da538366c7dfbe05da06ba5a96",
    "zh:fb6374e14b092f42cad8f8c8a4963d67b8654d58cbdd10ca9a175f83e9ecb943",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "2.3.2"
  hashes = [
    "h1:qxMfKMH8V4m+6oWbH8l/6LvvBJx5NEbJoXruL2OgNsI=",
    "zh:10f71c170be13538374a4b9553fcb3d98a6036bcd1ca5901877773116c3f828e",
    "zh:11d2230e531b7480317e988207a73cb67b332f225b0892304983b19b6014ebe0",
    "zh:3317387a9a6cc27fd7536b8f3cad4b8a9285e9461f125c5a15d192cef3281856",
    "zh:458a9858362900fbe97e00432ae8a5bef212a4dacf97a57ede7534c164730da4",
    "zh:50ea297007d9fe53e5411577f87a4b13f3877ce732089b42f938430e6aadff0d",
    "zh:56705c959e4cbea3b115782d04c62c68ac75128c5c44ee7aa4043df253ffbfe3",
    "zh:7eb3722f7f036e224824470c3e0d941f1f268fcd5fa2f8203e0eee425d0e1484",
    "zh:9f408a6df4d74089e6ce18f9206b06b8107ddb57e2bc9b958a6b7dc352c62980",
    "zh:aadd25ccc3021040808feb2645779962f638766eb583f586806e59f24dde81bb",
    "zh:b101c3456e4309b09aab129b0118561178c92cb4be5d96dec553189c3084dca1",
    "zh:ec08478573b4953764099fbfd670fae81dc24b60e467fb3b023e6fab50b70a9e",
  ]
}
