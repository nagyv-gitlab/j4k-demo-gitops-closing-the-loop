terraform {
  required_providers {
    civo = {
      source = "civo/civo"
      version = "0.10.4"
    }
  }
  backend "http" {
  }
}

# Configure the Civo Provider
provider "civo" {
  token = var.civo_token
  region = local.region
}

resource "civo_kubernetes_cluster" "dev-cluster" {
    name = "dev-cluster"
    // tags = "gitlab demo"
    network_id = data.civo_network.network.id
    applications = ""
    num_target_nodes = 3
    target_nodes_size = element(data.civo_instances_size.small.sizes, 0).name
}
