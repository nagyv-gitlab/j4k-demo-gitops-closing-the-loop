terraform {
  required_providers {
    civo = {
      source = "civo/civo"
      version = "0.10.4"
    }
  }
  backend "http" {
  }
}

# Configure the Civo Provider
provider "civo" {
  token = var.civo_token
  region = local.region
}

resource "civo_network" "network" {
    label = "development"
}
