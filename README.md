## J4K demo repository

### Prerequisites

- fork this repo
- create a `.local.env` file and add it to `.gitignore` with the following content

```
GITLAB_ACCESS_TOKEN=<your access token>
GITLAB_USER_LOGIN=<your GitLab username>
export TF_VAR_civo_token=<your Civo access token  # - this is optional see below>
```

### Setting up the cluster

You can use the Terraform projects under `terraform` to set up an EKS or a Civo cluster

### Register the GitLab Kubernetes Agent

You can use the Terraform project under `terraform/gitlab` or register the agent by hand

### Install the GitLab Kubernetes Agent

#### Grab the installation code

Follow [the docs](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/tree/master/build/deployment/gitlab-agent)

OR

```bash
kpt pkg get https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent.git/build/deployment/gitlab-agent gitlab-agent packages/gitlab-agent
kustomize cfg list-setters packages/gitlab-agent
kustomize cfg set packages/gitlab-agent agent-version v14.3.3
kustomize cfg set packages/gitlab-agent kas-address wss://kas.gitlab.com
```

#### Install the Agent

**Do not commit these changes! We write a secret to a file!** We will improve this later below.

```bash
cd terraform/gitlab/gitlab-agent
terraform output -raw token_secret > ../../../packages/gitlab-agent/base/secrets/agent.token  
cd ../../../
kustomize build packages/gitlab-agent/cluster | kubectl apply -f -
```

Finally, discard the changes.

### Play with CI

Add the following to `.gitlab-ci.yml`

```yaml
variables:
 KUBE_CONTEXT: "nagyv-gitlab/j4k-demo-gitops-closing-the-loop:j4k-demo"

.use_context: &use_context
  - kubectl config use-context "$KUBE_CONTEXT"

play:ci:
  stage: build
  image:
    name: bitnami/kubectl:latest
    entrypoint: [""]
  script:
  - *use_context
  - kubectl get pods --namespace gitlab-agent
```

#### Make a group level tunnel

1. Enable the agent for a group

```yaml
ci_access:
  # This agent is accessible from CI jobs in projects in these groups
  groups:
  - id: group/subgroup
```

1. Try it in a project under that group

### Turn the Agent to manage itself

We will use Bitnami Sealed Secrets to encrypt the agent token

#### Install Bitnami Sealed Secrets and grab the public key

- Add [the Kubernetes manifest](https://github.com/bitnami-labs/sealed-secrets/releases/latest) under the `kubernetes` directory
- Add the manifest path to `.gitlab/agents/j4k-demo/config.yaml`
- Grab the public key from the cluster and commit it

```bash
kubeseal --fetch-cert > sealed-secrets.pub.pem
```

#### Encrypt the Agent token

1. Create a Secret under the `ignored` directory

```bash
cd terraform/gitlab/gitlab-agent
terraform output -raw token_secret | kubectl create secret generic gitlab-agent-token -n gitlab-agent --dry-run=client --type=Opaque --from-file=token=/dev/stdin -o yaml > ../../../ignored/gitlab-agent-token.yaml
cd ../../../
```

1. Add annotations to it

```yaml
annotations:
  sealedsecrets.bitnami.com/managed: "true"
  sealedsecrets.bitnami.com/namespace-wide: "true"
```

1. Seal the secret and create manifests

```bash
bin/seal-secret.sh ignored/gitlab-agent-token.yaml packages/gitlab-agent/sealed-secret
kustomize build packages/gitlab-agent/sealed-secret > kubernetes/gitlab-agent.yaml
```

1. Enable manifest in agent config

#### Hydrate the package into manifests in CI

```yaml
include:
  - local: ".gitlab/ci/git_push.yaml"

gitops:hydrate:gitlab-agent:
  image: 
    name: line/kubectl-kustomize
    entrypoint: [""]
  stage: build
  script:
    - kustomize build packages/gitlab-agent/sealed-secret > kubernetes/gitlab-agent.yaml
    # - kustomize build packages/gitlab-agent/cluster-management > kubernetes/gitlab-agent.yaml
  artifacts:
    untracked: false
    expire_in: 1 days
    paths:
      - kubernetes/gitlab-agent.yaml
  rules:
    - changes:
      - packages/gitlab-agent/**/*
      - kubernetes/gitlab-agent.yaml
    - if: $ACTION == "packages"

gitops:annotate:
  image: alpine:latest
  stage: build
  script:
  - echo "Nothing to do"
  needs:
    - gitops:hydrate:gitlab-agent
  environment:
    name: production
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
      - packages/**/*
      - kubernetes/**/*

commit packages:
  extends: .git:push
  stage: deploy
  variables:
    COMMIT_MESSAGE: "Hydrated packages"
  script:
    cp -r kubernetes/* "${CI_COMMIT_SHA}/kubernetes/"
  dependencies:
    - "gitops:hydrate:gitlab-agent"
```

### Set up Prometheus

1. Adjust the RBAC in `packages/gitlab-agent/cluster-management/kustomization.yaml`
1. Apply the new RBAC locally

```bash
kustomize build packages/gitlab-agent/cluster-management > kubernetes/gitlab-agent.yaml
```

1. Commit the changes
1. Enable Prometheus in the `helmfile.yaml`
1. Configure the Ingress in `values.yaml`
1. Add an Ingress
1. Add it to GitLab

### Cleanup

You will be charged for the AWS resources.

- Terraform destroy
   1. `eks`
   1. `vpc`
